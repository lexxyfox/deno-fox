// https://www.w3.org/TR/DOM-Parsing
// https://html.spec.whatwg.org/multipage/dynamic-markup-insertion.html
// https://github.com/whatwg/domparsing

// This module is my promise to provide a standards compliant DOMParser.

export { DOMParser } from 'https://deno.land/x/deno_dom/deno-dom-wasm.ts'
