// https://dom.spec.whatwg.org
// https://github.com/whatwg/dom

// This module is my promise to provide standards compliant objects and interfaces as defined by the w3c.

export {
  Attr,
  CharacterData,
  Comment,
  DOMImplementation,
  DOMTokenList,
  Document,
  DocumentType,
  Element,
  HTMLCollection,
  NamedNodeMap,
  Node,
  NodeList,
  Text,
} from 'https://deno.land/x/deno_dom/deno-dom-wasm.ts'
