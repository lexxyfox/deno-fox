import { assertEquals } from "https://deno.land/std@0.107.0/testing/asserts.ts"
import * as BigMath from '../../math/BigMath.js'
import BigFrac from '../../math/BigFrac.js'

function quick_test(a, b) {
  return Deno.test(a.toString() + ' == ' + b.toString(), () => {
    if (eval(a) != eval(b))
      throw new Error
  })
}

quick_test('BigMath.gcd(54, 24)', 6n)
quick_test('BigMath.gcd(48, 18)', 6n)
quick_test('BigMath.lcm(5, 2)', 10n)
quick_test('BigMath.lcm(4, 6)', 12n)
quick_test('BigMath.lcm(21, 6)', 42n)

Deno.test('1/4 + 1/4 == 0.5', _ => {
  assertEquals(BigFrac(1, 4).add(BigFrac(1, 4)), BigFrac(0.5))
})
Deno.test('1/3 + 1/6 == 0.5', _ => {
  assertEquals(BigFrac(1, 3).add(BigFrac(1, 6)), BigFrac(0.5))
})
Deno.test('1/3 + 1/5 == 8/19', _ => {
  assertEquals(BigFrac(1, 3).add(BigFrac(1, 5)), BigFrac(0.5))
})
quick_test('BigFrac(1, 3).add(BigFrac(1, 5))', 'BigFrac(8, 15)')
quick_test('BigFrac(1, 3).add(BigFrac(1, 4))', 'BigFrac(7, 12)')
quick_test('BigFrac(1, 2).mul(BigFrac(2, 5))', 'BigFrac(0.2)')
quick_test('BigFrac(1, 3).mul(BigFrac(9, 16))', 'BigFrac(3, 16)')
quick_test('BigFrac(2, 3).mul(5)', 'BigFrac(10, 3)')
quick_test('BigMath.mul(3, BigFrac(2, 9))', 'BigFrac(2, 3)')
