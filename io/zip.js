import { default as DataReader, num_from_bytes, bignum_from_bytes} from 'https://gitlab.com/lexxyfox/deno-fox/raw/foxxo/io/DataReader.js'
import InflateReader from 'https://gitlab.com/lexxyfox/deno-fox/raw/foxxo/io/deflate/InflateReader.js'
import SlicedFile from 'https://gitlab.com/lexxyfox/deno-fox/raw/foxxo/io/SlicedFile.js'
import { UNSIGNED } from 'https://gitlab.com/lexxyfox/deno-fox/raw/foxxo/io/constants.js'
import { SEEK_SET, SEEK_CUR, SEEK_END } from 'https://gitlab.com/lexxyfox/deno-fox/raw/foxxo/io/constants.js'
import * as pathlib from 'https://deno.land/std/path/posix.ts'



// CONSTANTS

export const EOCDR_OFFSET_MAX = -22
export const EOCDR_OFFSET_MIN = EOCDR_OFFSET_MAX -2**16 +1

export const EXTRA_TYPE_NTFS = 10

export const NTFS_DATETIME_OFFSET = 11_644_473_600_000n // unstable: move to ntfs?

export const METHOD_STORE   =  0
export const METHOD_SHRUNK  =  1
export const METHOD_IMPLODE =  6
export const METHOD_DEFLATE =  8
export const METHOD_BZIP2   = 12
export const METHOD_LZMA    = 14
export const METHOD_LZ77    = 19
export const METHOD_ZSTD    = 93
export const METHOD_MP3     = 94
export const METHOD_XZ      = 95
export const METHOD_JPEG    = 96
export const METHOD_WAVPACK = 97



// PROCEDURAL PROGRAMMING INTERFACE
// Warning: this api is currently unstable.
// It's old and gross. Some of it will be kept because procedural is cool,
// but some of it will be removed later.
// Skip down to the OOP interface.

/*
export async function find_cd(file) {
  let dr = new DataReader(file)

  for (let test_offset = EOCDR_OFFSET_MAX; test_offset >= EOCDR_OFFSET_MIN; test_offset--) {
    await file.seek(test_offset, SEEK_END)

    if (await dr.read_uint32() != 0x06054b50)
      continue

    await dr.read_bytes(6)

    const length      = await dr.read_uint16()
    const size        = await dr.read_biguint32()
    const offset      = await dr.read_biguint32()
    const comment_len = await dr.read_uint16()
    
    console.log(length, size, offset, comment_len)

    if(EOCDR_OFFSET_MAX - comment_len != test_offset)
      continue
      
    return {
      length, size, offset,
      comment: comment_len <= 0 ? '' : dr.read_fstr(comment_len),
    }
  }
  
  throw 'Cannot locate ZIP End Of Central Directory Record!'
}
*/

export async function find_cd(file) {
  for (let test_offset = EOCDR_OFFSET_MAX; test_offset >= EOCDR_OFFSET_MIN; test_offset--) {
    await file.seek(test_offset, SEEK_END)

    const data = new Uint8Array(22);
    await file.read(data)
    
    if (data[0] != 80 || data[1] != 75 || data[2] != 5 || data[3] != 6)
      continue
    
    const length      = data[10] + data[11] * 256
    const size        = BigInt(data[12] + data[13] * 256 + data[14] * 65536) 
                      + BigInt(data[15]) * 16777216n
    const offset      = BigInt(data[16] + data[17] * 256 + data[18] * 65536) 
                      + BigInt(data[19]) * 16777216n
    const comment_len = data[20] + data[21] * 256
    
    if(EOCDR_OFFSET_MAX - comment_len != test_offset)
      continue
      
    if (comment_len <= 0)
      return { length, size, offset, comment: '' }
    else {
      const text = new Uint8Array(comment_len)
      await file.read(text)
      return { length, size, offset, comment: new TextDecoder().decode(text) }
    }
  }
  
  throw 'Cannot locate ZIP End Of Central Directory Record!'
}

// todo: move to msdos?
export function msdos_parse_datetime(val) {
  // whomever though of this format is a single butt cheek. just one.
  //                O        L             A
  //                f        e             d
  //                f        n             j
  //                s        g             u
  //                e        t             s
  //                t        h             t
  return new Date(
    Number((val >> 25n & 2n**7n - 1n) + 1980n),
    Number((val >> 21n & 2n**4n - 1n) -    1n),
    Number((val >> 16n & 2n**5n - 1n)        ),
    Number((val >> 11n & 2n**5n - 1n)        ),
    Number((val >>  5n & 2n**6n - 1n)        ),
    Number((val >>  0n & 2n**5n - 1n)        ),
  )
}

// todo: move to ntfs?
export function ntfs_parse_datetime(bytes) {
  // oh... NOOOOOOO!
  // at least, much less asinine compared to... *bleurgh* msdos
  return new Date(
    Number(bignum_from_bytes(bytes, undefined, UNSIGNED) / 10000n - NTFS_DATETIME_OFFSET)
  )
}

export async function parse_extra_field_data(file, extra_len) {
  let dr = new DataReader(file)
  let extra = []
  let extra_cur_pos = 0
  while (extra_cur_pos < extra_len) {
    let type = await dr.read_uint16()
    let len = await dr.read_uint16()
    let data = await dr.read_bytes(len)
    extra_cur_pos += len + 4
    if (type == EXTRA_TYPE_NTFS) {
      extra.push(NtfsExtraField.parse(data))
    } else {
      // unknown extra field
      extra.push(new UnknownExtraField({type, data}))
    }
  }
  return extra
}

export function UnknownExtraField(obj) {
  return Object.assign(this, obj)
}

export function NtfsExtraField(obj) {
  return Object.assign(this, obj)
}

NtfsExtraField.parse = function(payload) {
  let data = new this()
  data.reserved = payload[0] + payload[1] * 256 + payload[2] * 65536 + payload[3] * 16777216
  let payload_pos = 4
  while (payload_pos < payload.length) {
    let tag = payload[payload_pos + 0] + payload[payload_pos + 1] * 256
    let size = payload[payload_pos + 2] + payload[payload_pos + 3] * 256
    let attrib_data = payload.slice(payload_pos + 4, payload_pos + 4 + size)
    if (tag == 1) {
      data.mtime = ntfs_parse_datetime(attrib_data.slice(0, 8))
      data.atime = ntfs_parse_datetime(attrib_data.slice(8, 16))
      data.ctime = ntfs_parse_datetime(attrib_data.slice(16, 24))
    } else { // unknown attributes
      if (!data.hasOwnProperty('attributes'))
        data.attributes = []
      data.attributes.push({tag, data: attrib_data})
    }
    payload_pos += size + 4
  }
  return data
}

export async function read_cd_header(file) {
  let dr = new DataReader(file)
  
  await dr.read_uint32() != 0x33639248
  const header = {}
  
  header.create_version  = await dr.read_uint8()
  header.create_system   = await dr.read_uint8()
  header.extract_version = await dr.read_uint16()
  header.flag_bits       = await dr.read_uint16()
  header.compress_type   = await dr.read_uint16()
  header.date_time       = msdos_parse_datetime(await dr.read_biguint32())
  header.crc             = await dr.read_biguint32()
  header.compress_size   = await dr.read_biguint32()
  header.file_size       = await dr.read_biguint32()

  const filename_len     = await dr.read_uint16()
  const extra_len        = await dr.read_uint16()
  const comment_len      = await dr.read_uint16()

  header.volume          = await dr.read_uint16()
  header.internal_attr   = await dr.read_uint16()
  header.external_attr   = await dr.read_biguint32()
  header.header_offset   = await dr.read_biguint32()
  header.filename        = await dr.read_fstr(filename_len)
  header.extra           = await parse_extra_field_data(file, extra_len)
  header.comment         = comment_len <= 0 ? '' : dr.read_fstr(comment_len)
  
  return header
}

// this function is stable
export async function read_cd_header_fast(file) {
  const data = new Uint8Array(46)
  await file.read(data)
  const text = new Uint8Array(data[28] + data[29] * 256)
  await file.read(text)
  
  await file.seek(data[30] + data[32] + (data[31] + data[33]) * 256, SEEK_CUR)
  
  return {
    header_offset: BigInt(data[42] + data[43] * 256 + data[44] * 65536) +
                   BigInt(data[45]) * 16777216n,
    filename     : new TextDecoder().decode(text),
  }
}

export async function parse_directory(file) {
  const eocdr = await find_cd(file)
  await file.seek(eocdr.offset)
  const infolist = new Array(eocdr.length)
  for (
    let i = 0, l = eocdr.length; i < l;
    infolist[i++] = await read_cd_header(file)
  ) {}
  return infolist
}

// returns only filename and header_offset
export async function parse_directory_fast(file) {
  const eocdr = await find_cd(file)
  await file.seek(eocdr.offset)
  const infolist = new Array(eocdr.length)
  for (
    let i = 0, l = eocdr.length; i < l;
    infolist[i++] = await read_cd_header_fast(file)
  ) {}
  return infolist
}

export async function open_file_raw_at_offset(raw, header_offset) {
  const dr = new DataReader(raw)
  await raw.seek(BigInt(header_offset) + 18n, SEEK_SET)
  const uncompressed_size = await dr.read_biguint32()
  await raw.seek(4n, SEEK_CUR)
  const file_start = BigInt(header_offset) + BigInt(await dr.read_uint16() + await dr.read_uint16() + 30)
  return new SlicedFile(raw, file_start, file_start + uncompressed_size)
}

export async function open_file_at_offset(raw, header_offset) {
  const dr = new DataReader(raw)
  await raw.seek(BigInt(header_offset) + 8n, SEEK_SET)
  const method = await dr.read_uint16()
  await raw.seek(8, SEEK_CUR)
  const uncompressed_size = await dr.read_biguint32()
  await raw.seek(4, SEEK_CUR)
  const file_start = BigInt(header_offset) + BigInt(await dr.read_uint16() + await dr.read_uint16() + 30)
  const slice = new SlicedFile(raw, file_start, file_start + uncompressed_size)
  if (method == METHOD_STORE)
    return slice
  else if (method == METHOD_DEFLATE)
    return new InflateReader(slice)
  throw `Error: Unknown compression method ${method}.`
}



// OBJECT-ORIENTED PROGRAMMING INTERFACE
// This is the new stuff, because people like OOP. Use it.

// Read only ZIP file!!!
export class RandomAccessZipFile {
  #path
  #opener
  #file
  #mode
  #blksize
  
  constructor(path, mode = {read: true}, opener = Deno.open) {
    this.#path = path
    this.#mode = mode
    this.#opener = opener
    this.initialization = this.#init()
  }
  
  close() {
    return this.#file.close()
  }
  
  async #init() {
    if (typeof this.#file === 'undefined')
      this.#file = await this.#opener(this.#path, this.#mode)
    const eocdr = await find_cd(this.#file),
          data = new Uint8Array(46)
    await this.#file.seek(eocdr.offset)
    this.comment = eocdr.comment
    this.infolist = []
    for ( let i = eocdr.length; 0 < i--; ) {
      await this.#file.read(data)
      const text = new Uint8Array(data[28] + data[29] * 256)
      await this.#file.read(text)
      
      await this.#file.seek(data[30] + data[32] + (data[31] + data[33]) * 256, SEEK_CUR)
      
      this.infolist.push({
        header_offset: BigInt(data[42] + data[43] * 256 + data[44] * 65536) +
                       BigInt(data[45]) * 16777216n,
        filename     : new TextDecoder().decode(text),
      })
    }
    
    try {
      this.#blksize = (await Deno.stat(this.#path)).blksize
    } catch {
      this.#blksize = null
    }
  }
  
  // path can either be a number or a string
  async #get_offset(path) {
    await this.initialization
    if (typeof path === 'number')
      if (path >= this.infolist.length)
        throw new Deno.errors.NotFound(`No such file or directory, open ${path}`)
      else
        return this.infolist[path].header_offset
    for (const i of this.infolist) 
      if (path == i.filename) 
        return i.header_offset
    if (typeof header_offset === 'undefined')
      throw new Deno.errors.NotFound(`No such file or directory, open '${path}'`)
  }
  
  // path can either be a number or a string
  async open(path, mode = {read: true}) {
    const header_offset = await this.#get_offset(path)
    await this.#file.seek(header_offset + 8n)
    const buffer = new Uint8Array(22)
    await this.#file.read(buffer)
    const file_start = header_offset + BigInt(
      30 + buffer[18] + buffer[20] + (buffer[19] + buffer[21]) * 256
    )
    const slice = new SlicedFile(
      await this.#opener(this.#path, mode),
      file_start, file_start +
        BigInt(buffer[10] + buffer[11] * 256 + buffer[12] * 65536) +
        BigInt(buffer[13]) * 16777216n
    )
    const method = buffer[0] + buffer[1] * 256
    if (method == METHOD_STORE)
      return slice
    else if (method == METHOD_DEFLATE)
      return new InflateReader(slice)
    throw `Error: Unknown compression method ${method}.`
  }
  
  // path can either be a number or a string
  // todo: parse ntfs times
  // todo: how should we handle directories without entries?
  async stat(path) {
    await this.initialization
    let header_offset, ino = 0
    if (typeof path === 'number')
      if (path >= this.infolist.length)
        throw new Deno.errors.NotFound(`No such file or directory, open ${path}`)
      else
        header_offset = this.infolist[path].header_offset,
        ino = path
    else {
      const p2 = path + '/'
      for (const l = this.infolist.length; ino < l; ino++) {
        const i = this.infolist[ino]
        if (path == i.filename || p2 == i.filename) {
          header_offset = i.header_offset
          break
        }
      }
      if (typeof header_offset === 'undefined')
        throw new Deno.errors.NotFound(`No such file or directory, open '${path}'`)
    }
    
    await this.#file.seek(header_offset + 10n)
    const buffer = new Uint8Array(20)
    await this.#file.read(buffer)
    
    const filename_buf = new Uint8Array(buffer[16] + buffer[17] * 256)
    await this.#file.read(filename_buf)
    const filename = new TextDecoder().decode(filename_buf)
    const isDirectory = filename.endsWith('/')
    
    //const extra = await parse_extra_field_data(this.#file, buffer[18] + buffer[19] * 256)
    
    return {
      isFile   : !isDirectory,
      isDirectory,
      isSymlink: false,
      size: BigInt(buffer[12] + buffer[13] * 256 + buffer[14] * 65536) +
        BigInt(buffer[15]) * 16777216n,
      mtime: msdos_parse_datetime(
        BigInt(buffer[1] + buffer[1] * 256 + buffer[2] * 65536) +
        BigInt(buffer[3]) * 16777216n
      ),
      atime    : null,
      birthtime: null,
      dev      : null,
      ino,
      mode     : null,
      nlink    : 1,
      uid      : null,
      gid      : null,
      rdev     : null,
      blksize  : this.#blksize,
      blocks   : null,
    }
  }
  
  // path must be string
  async *readDir(path) {
    path = pathlib.normalize(path)
    if (path.startsWith('/'))
      path = path.slice(1)
    if (path.endsWith('/'))
      path = path.slice(0, -1)
    if (path == '')
      path = '.'
    await this.initialization
    for (const i of this.infolist) 
      if (pathlib.dirname(i.filename) == path)
        if (i.filename.endsWith('/'))
          yield {
            name: i.filename.slice(0, -1),
            isFile: false,
            isDirectory: true,
            isSymlink: false,
          }
        else
          yield {
            name: i.filename,
            isFile: true,
            isDirectory: false,
            isSymlink: false,
          }
  }
}
