import {BIG_ENDIAN, LITTLE_ENDIAN, UNSIGNED, TWOS_COMPLEMENT, HALF_BIAS} from './constants.js'

export const num_from_bytes = (bytes, byteorder = LITTLE_ENDIAN, signed = TWOS_COMPLEMENT) => {
  let num = 0, pos = 0
  if (byteorder == BIG_ENDIAN) 
    for (let b of bytes)
      num = num * 256 + b
  else if (byteorder == LITTLE_ENDIAN) 
    for (let b of bytes)
      num += b << (pos++ * 8)
  else
    throw 'Unsupported endianness'
  if (signed == UNSIGNED)
    return num
  else if (signed == TWOS_COMPLEMENT)
    return num < 2**(bytes.length * 8 - 1) ? num : num - 2**(bytes.length * 8)
  else if (signed == HALF_BIAS)
    return num - 2**(bytes.length * 8 - 1)
  throw 'Unsuppored signedness'
}

export const bignum_from_bytes = (bytes, byteorder = LITTLE_ENDIAN, signed = TWOS_COMPLEMENT) => {
  let num = 0n, pos = 0
  if (byteorder == BIG_ENDIAN) 
    for (let b of bytes)
      num = num * 256n + BigInt(b)
  else if (byteorder == LITTLE_ENDIAN) 
    for (let b of bytes)
      num += BigInt(b) << BigInt(pos++ * 8)
  else
    throw 'Unsupported endianness'
  if (signed == UNSIGNED)
    return num
  else if (signed == TWOS_COMPLEMENT)
    return num < 2n**(BigInt(bytes.length) * 8n - 1n) ? num : num - 2n**(BigInt(bytes.length) * 8n)
  else if (signed == HALF_BIAS)
    return num - 2n**(BigInt(bytes.length) * 8n - 1n)
  throw 'Unsuppored signedness'
}

export default class DataReader {
  #raw
  constructor(raw, byteorder, text_encoding) {
    this.#raw = raw
    this.byteorder = byteorder
    this.text_encoding = text_encoding
  }
  
  async read_all(size = 8192) {
    let bytes_read_so_far = 0
    let bytes_read_this_time = 0
    let buf = new Uint8Array()
    let chunk = new Uint8Array(size)
    while ((bytes_read_this_time = await this.#raw.read(chunk)) > 0) {
      const new_buf = new Uint8Array(buf.length + chunk.length)
      new_buf.set(buf, 0)
      new_buf.set(chunk, bytes_read_so_far)
      buf = new_buf
      bytes_read_so_far += bytes_read_this_time
    }
    return bytes_read_so_far == buf.length ? buf : buf.slice(0, bytes_read_so_far)
  }
  async read_bytes(size = -1) {
    if (size < 0)
      return this.read_all()
    const buf = new Uint8Array(size)
    const real_size = (await this.#raw.read(buf)) || 0
    return real_size == size ? buf : buf.slice(0, real_size)
  }
  close() {
    this.#raw.close()
  }
  
  // Small Int functions
  async read_int(size, byteorder, signed = TWOS_COMPLEMENT) {
    return num_from_bytes(await this.read_bytes(size), byteorder ?? this.byteorder, signed)
  }
  async read_uint(size, byteorder, signed = UNSIGNED) {
    return num_from_bytes(await this.read_bytes(size), byteorder ?? this.byteorder, signed)
  }
  async read_int8(byteorder) {
    return num_from_bytes(await this.read_bytes(1), byteorder ?? this.byteorder, TWOS_COMPLEMENT)
  }
  async read_uint8(byteorder) {
    return num_from_bytes(await this.read_bytes(1), byteorder ?? this.byteorder, UNSIGNED)
  }
  async read_int16(byteorder) {
    return num_from_bytes(await this.read_bytes(2), byteorder ?? this.byteorder, TWOS_COMPLEMENT)
  }
  async read_uint16(byteorder) {
    return num_from_bytes(await this.read_bytes(2), byteorder ?? this.byteorder, UNSIGNED)
  }
  async read_int32(byteorder) {
    return num_from_bytes(await this.read_bytes(4), byteorder ?? this.byteorder, TWOS_COMPLEMENT)
  }
  async read_uint32(byteorder) {
    return num_from_bytes(await this.read_bytes(4), byteorder ?? this.byteorder, UNSIGNED)
  }
  
  // Big Int functions
  async read_bigint(size, byteorder, signed = TWOS_COMPLEMENT) {
    return bignum_from_bytes(await this.read_bytes(size), byteorder ?? this.byteorder, signed)
  }
  async read_biguint(size, byteorder, signed = UNSIGNED) {
    return bignum_from_bytes(await this.read_bytes(size), byteorder ?? this.byteorder, signed)
  }
  async read_bigint8(byteorder) {
    return BigInt(await this.read_int8(byteorder))
  }
  async read_biguint8(byteorder) {
    return BigInt(await this.read_uint8(byteorder))
  }
  async read_bigint16(byteorder) {
    return BigInt(await this.read_int16(byteorder))
  }
  async read_biguint16(byteorder) {
    return BigInt(await this.read_uint16(byteorder))
  }
  async read_bigint32(byteorder) {
    return bignum_from_bytes(await this.read_bytes(4), byteorder ?? this.byteorder, TWOS_COMPLEMENT)
  }
  async read_biguint32(byteorder) {
    return bignum_from_bytes(await this.read_bytes(4), byteorder ?? this.byteorder, UNSIGNED)
  }
  async read_bigint64(byteorder) {
    return bignum_from_bytes(await this.read_bytes(8), byteorder ?? this.byteorder, TWOS_COMPLEMENT)
  }
  async read_biguint64(byteorder) {
    return bignum_from_bytes(await this.read_bytes(8), byteorder ?? this.byteorder, UNSIGNED)
  }
  async read_bigint128(byteorder) {
    return bignum_from_bytes(await this.read_bytes(16), byteorder ?? this.byteorder, TWOS_COMPLEMENT)
  }
  async read_biguint128(byteorder) {
    return bignum_from_bytes(await this.read_bytes(16), byteorder ?? this.byteorder, UNSIGNED)
  }
  
  // Fixed length string functions
  async read_fstr(size, encoding) {
    return new TextDecoder(encoding ?? this.encoding).decode(await this.read_bytes(size))
  }
  async read_pstr(encoding, size = 2, byteorder, signed) {
    return this.read_fstr(await this.read_uint(...[].slice.call(arguments, 1)), encoding)
  }
  
  // Variable length string functions
  async read_cstr(encoding) {
    let str = []
    for (
        let buffer = new Uint8Array(1),
            read = 0, byte = 0;
        read = (await this.#raw.read(buffer)),
        byte = buffer[0],
        read > 0 && byte > 0; 
      )
      str.push(byte)
    return new TextDecoder(encoding ?? this.encoding).decode(new Uint8Array(str))
  }
}
