import * as stdio from 'https://deno.land/std/io/util.ts'
import {SEEK_SET, SEEK_CUR, SEEK_END} from '../constants.js'
import {LC_DEF, LP_DEF, PB_DEF, DICT_DEF} from './constants.js'
import {LITTLE_ENDIAN} from '../constants.js'
import DataReader from '../DataReader.js'

// Please help convert this to native JS!

export default class RawLZMAReader {
  #raw
  #proc
  #stdout
  #opened
  #closed
  #cmd
  #pos
  constructor(raw, opts, ver = 1) {
    opts = Object.assign({
      lc: LC_DEF,
      lp: LP_DEF,
      pb: PB_DEF,
      dict: DICT_DEF,
    }, opts)
    // Yes, I really do want to sanitize and shorten the command AMAP
    let first = false
    this.#cmd = `--lzma${parseInt(ver)}`,
      first = true
    if (opts.lc != LC_DEF)
      this.#cmd += (first ? '=' : ',') + `lc=${parseInt(opts.lc)}`,
      first = false
    if (opts.lp != LP_DEF)
      this.#cmd += (first ? '=' : ',') + `lp=${parseInt(opts.lp)}`,
      first = false
    if (opts.pb != PB_DEF)
      this.#cmd += (first ? '=' : ',') + `pb=${parseInt(opts.pb)}`,
      first = false
    if (opts.dict != DICT_DEF)
      this.#cmd += (first ? '=' : ',') + `dict=${parseInt(opts.dict)}`,
      first = false
    this.#pos = 0
    this.#raw = raw
    this.#opened = false
    this.#closed = false
  }
  tell() {
    return this.#pos
  }
  close() {
    // stdin does NOT need to be closed here, 
    // but does stdout need to be?
    if (!this.#closed)
      this.#proc.close()
    this.#closed = true
  }
  #start() {
    this.#proc = Deno.run({
      cmd: ['xz', '-dqFraw', this.#cmd],
      stdin: 'piped',
      stdout: 'piped',
      stderr: 'piped',
    })
    stdio.copy(this.#raw, this.#proc.stdin).catch(async err => {
      const err_text = (new TextDecoder().decode(await this.#proc.stderrOutput())).substr(13)
      if (err_text.length > 0)
        throw new Error(err_text)
    }).then(
      _ => this.#proc.stdin.close()
    )
    this.#opened = true
    this.#stdout = this.#proc.stdout
  }
  async read(buffer) {
    if (!this.#opened)
      this.#start()
    const bytes_read = await this.#stdout.read(buffer)
    if (bytes_read === null)
      this.close()
    this.#pos += bytes_read
    return bytes_read
  }
  async skip_bytes(size = -1) {
    if (size == 0)
      return 0
    else if (size < 0) {
      throw new Error('Not yet implemented!')
    } else if (size > 0) {
      let so_far = 0, b = 0
      while (b = await this.read(new Uint8Array(size - so_far))) {
        if (b == 0 || b == null)
          break
        so_far += b
      }
      return so_far
    }
  }
  async seek(offset, whence = SEEK_SET) {
    if (whence == SEEK_SET)
      if (offset < this.#pos) // todo: this is possible
        throw new Error('Illegal seek')
      else if (offset == this.#pos)
        return this.#pos
      else {
        await this.skip_bytes(offset - this.#pos)
        return this.#pos
      }
    else if (whence == SEEK_CUR)
      if (offset < 0) // todo: this is possible
        throw new Error('Illegal seek')
      else if (offset == 0)
        return this.#pos
      else {
        await this.skip_bytes(offset)
        return this.#pos
      }
    else if (whence == SEEK_END)
      throw new Error('Illegal seek')
    throw new TypeError(`Invalid seek mode: ${whence}`)
  }
  // for use with the larger 21 byte header format
  // 1B props + 4B dict size + 8B uncompressed size + 8B compressed size = 21
  static async from_lzma_owo_stream(raw) {
    const reader = new DataReader(raw, LITTLE_ENDIAN)
    let pb = await reader.read_uint8()
    const lc = pb % 9
    pb = Math.floor(pb / 9)
    const lp = pb % 5
    pb = Math.floor(pb / 5)
    const dict = await reader.read_uint32()
    const lzma_reader = new this(raw, {
      lc: lc,
      lp: lp,
      pb: pb,
      dict: dict
    })
    lzma_reader.size_uncompressed = await reader.read_biguint64()
    lzma_reader.size_compressed = await reader.read_biguint64()
    return lzma_reader
  }
}
