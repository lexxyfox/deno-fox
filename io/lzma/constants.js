export const LC_MIN = 0
export const LC_DEF = 3
export const LC_MAX = 4

export const LP_MIN = 0
export const LP_DEF = 0
export const LP_MAX = 4

export const PB_MIN = 0
export const PB_DEF = 2
export const PB_MAX = 4

export const DICT_MIN = 2**(3* 4)
export const DICT_DEF = 2**(3* 7)
export const DICT_MAX = 2**(3*10) * 1.5
