import {SEEK_SET, SEEK_CUR, SEEK_END} from './constants.js'

// A really bad alternative to Deno's readerFromStreamReader().
export default class BufferedWebReader {
  #raw
  #buf
  #buf_pos
  #pos
  constructor(raw) {
    this.#raw = raw
    this.#pos = 0
    this.#buf_pos = 0
    this.#buf = new Uint8Array()
  }
  close() {
    this.#raw.cancel()
  }
  tell() {
    return this.#pos
  }
  static from_response_body(body) {
    return new this(body.getReader())
  }
  static async from_response(response) {
    return this.from_response_body((await response).body)
  }
  // I wanted something faster than copying to a buffer and discarding it
  // for use for example with skip()
  // This can probably be improved a lot...
  async skip_bytes(size = -1) {
    if (size == 0)
      return 0
    let done = false,
      skipped_so_far = 0
    if (size < 0) {
      while (!done) {
        skipped_so_far += this.#buf.byteLength - this.#buf_pos
        const r = await this.#raw.read()
        done = r.done
        if (!done) {
          this.#buf = r.value
        }
      }
      this.#buf_pos = this.#buf.byteLength
    } else
      while (!done && skipped_so_far < size) {
        const skipped = Math.min(size, this.#buf.byteLength - this.#buf_pos)
        skipped_so_far += skipped
        if (skipped_so_far < size) {
          const r = await this.#raw.read()
          done = r.done
          if (!done)
            this.#buf = r.value,
            this.#buf_pos = 0
          else
            this.#buf_pos += skipped
        } else
          this.#buf_pos += skipped
      }
    this.#pos += skipped_so_far
    return skipped_so_far
  }
  async read1(p, off = 0) {
    let done = false
    if (this.#buf_pos >= this.#buf.byteLength) {
      const r = await this.#raw.read()
      done = r.done
      if (!done)
        this.#buf = r.value,
        this.#buf_pos = 0
    }
    if (done || this.#buf.byteLength <= 0) 
      return null
    const bytes_to_read = Math.min(p.length - off, this.#buf.length - this.#buf_pos)
    p.set(new Uint8Array(this.#buf.buffer, this.#buf_pos, bytes_to_read), off)
    this.#buf_pos += bytes_to_read
    this.#pos += bytes_to_read
    return bytes_to_read
  }
  async read(p) {
    let bytes_so_far = await this.read1(p)
    if (bytes_so_far == null)
      return null
    if (bytes_so_far >= p.byteLength)
      return bytes_so_far
    return bytes_so_far + (await this.read1(p, bytes_so_far) || 0)
  }
  async seek(offset, whence = SEEK_SET) {
    if (whence == SEEK_SET)
      if (offset < this.#pos) // todo: this is possible
        throw new Error('Illegal seek')
      else if (offset == this.#pos)
        return this.#pos
      else {
        this.skip_bytes(offset - this.#pos)
        return this.#pos
      }
    else if (whence == SEEK_CUR)
      if (offset < 0) // todo: this is possible
        throw new Error('Illegal seek')
      else if (offset == 0)
        return this.#pos
      else {
        this.skip_bytes(offset)
        return this.#pos
      }
    else if (whence == SEEK_END) {
      if (offset > 0)
        throw new Error('Illegal seek')
      await this.skip_bytes()
      if (offset == 0)
        return this.#pos
      if (offset * -1 <= this.#buf.byteLength) {
        this.#buf_pos = this.#buf.byteLength + offset
        return this.#pos += offset
      }
      throw new Error('Illegal seek')
    }
    throw new TypeError(`Invalid seek mode: ${whence}`)
  }
}
