import { SEEK_SET, SEEK_CUR, SEEK_END } from 'https://gitlab.com/lexxyfox/deno-fox/-/raw/foxxo/io/constants.js'

// todo: support for slices without beginning
// todo: support for slices without end
export default class SlicedFile {
  #raw
  #start
  #end
  #pos
  #has_seeked_to_start
  constructor(raw, start = 0, end) {
    this.#raw = raw
    if (end < start)
      throw new RangeError('end must be >= start')
    this.#start = BigInt(start)
    this.#end = BigInt(end)
    this.#pos = 0n
  }
  
  close() {
    return this.#raw.close()
  }
  
  async read(p) {
    if (this.#start + this.#pos >= this.#end)
      return null
    if (!this.#has_seeked_to_start)
      await this.seek(0)
    if (this.#start + this.#pos + BigInt(p.length) > this.#end) {
      const new_len = this.#end - this.#start - this.#pos
      const new_buf = new Uint8Array(Number(new_len));
      const copied = await this.#raw.read(new_buf)
      this.#pos += BigInt(copied)
      p.set(new_buf)
      return copied
    }
    const copied = await this.#raw.read(p)
    this.#pos += BigInt(copied)
    return copied
  }
  
  // todo: support for slices without beginning
  // todo: support for slices without end
  async seek(offset, whence = SEEK_SET) {
    offset = BigInt(offset)
    if (whence == SEEK_SET) {
      if (offset < 0)
        throw new Error('Illegal seek')
      let new_pos = this.#start + offset
      if (new_pos > this.#end) // is this correct? or should we move to end?
        throw new Error('Illegal seek')
      new_pos = BigInt(await this.#raw.seek(new_pos)) // p might not be = new_pos
      this.#has_seeked_to_start = true
      this.#pos = new_pos - this.#start // can this be combined with line below?
      return this.#pos 
    } else if (whence == SEEK_END) {
      if (offset > 0)
        throw new Error('Illegal seek')
      let new_pos = this.#end + offset
      if (new_pos < 0) 
        throw new Error('Illegal seek')
      new_pos = BigInt(await this.#raw.seek(new_pos)) // p might not be = new_pos
      this.#has_seeked_to_start = true
      this.#pos = new_pos - this.#start // can this be combined with line below?
      return this.#pos
    } else if (whence == SEEK_CUR)
        return this.seek(offset + this.#pos, SEEK_SET)
    throw new TypeError(`Invalid seek mode: ${whence}`)
  }
}
