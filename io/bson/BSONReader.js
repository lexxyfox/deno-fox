import DataReader from '../DataReader.js'
import IEEEReader from '../IEEE754Reader.js'
import {DOC_END, BIN64, STRING, OBJECT, ARRAY, BOOLEAN, NULL, INT64} from './constants.js'
import {SEEK_CUR} from '../constants.js'

export default class BSONReader {
  #raw
  #data_reader
  #ieee_reader
  constructor(raw) {
    this.#raw = raw
    this.#data_reader = new DataReader(raw)
    this.#ieee_reader = new IEEEReader(raw)
    this.path = []
    this.last_type = DOC_END
  }
  async read_document_header() {
    return (await this.#data_reader.read_uint32()) - 4
  }
  async read_next() {
    const type = (await this.#data_reader.read_bytes(1))[0]
    const name = type > DOC_END ? await this.#data_reader.read_cstr() : null
    this.last_type = type
    const ret = {type, path: [...this.path, name]}
    if (type == OBJECT || type == ARRAY)
      this.path.push(name)
    else if (type == DOC_END)
      this.path.pop()
    return ret
  }
  async read_boolean() {
    const val = (await this.#data_reader.read_bytes(1))[0]
    if (val == 0)
      return false
    if (val == 1)
      return true
    return val
  }
  async read_value() {
    if (this.last_type == DOC_END)
      return null
    else if (this.last_type == BIN64)
      return this.#ieee_reader.read_binary(8)
    else if (this.last_type == STRING)
      return this.#data_reader.read_pstr(undefined, 4)
    else if (this.last_type == OBJECT || this.last_type == ARRAY)
      return this.read_document_header()
    else if (this.last_type == BOOLEAN)
      return this.read_boolean()
    else if (this.last_type == NULL)
      return null
    else if (this.last_type == INT64)
      return this.#data_reader.read_bigint64()
    throw new Error(`Unimplemented reading of BSON type ${this.last_type}`)
  }
  async skip_value() {
    if (this.last_type == DOC_END)
      return null
    if (this.last_type == BIN64)
      await this.#raw.seek(8, SEEK_CUR)
    else if (this.last_type == STRING)
      await this.#data_reader.read_pstr(undefined, 4)
    else if (this.last_type == OBJECT || this.last_type == ARRAY)
      await this.#raw.seek(this.read_document_header(), SEEK_CUR)
    else if (this.last_type == BOOLEAN)
      await this.#raw.seek(1, SEEK_CUR)
    else if (this.last_type == NULL)
      return null
    else if (this.last_type == INT64)
      await this.#raw.seek(4, SEEK_CUR)
    else
      throw new Error(`Unimplemented skipping of BSON type ${this.last_type}`)
  }
}




