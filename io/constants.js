// Consistent with Python, Deno, et al.
export const SEEK_SET = 0
export const SEEK_CUR = 1
export const SEEK_END = 2

// Stolen from glibc's <endian.h>
export const BIG_ENDIAN    = 4321
export const LITTLE_ENDIAN = 1234
export const PDP_ENDIAN    = 4312

// signedness constants
// AFAIK, original creation, and...
// not gonna lie, I kinda hate these values. pls hlep!
export const UNSIGNED = 0
export const TWOS_COMPLEMENT = 1
export const SIGN_MAGNITUDE = 2
export const ONES_COMPLEMENT = 3
export const HALF_BIAS = 4
export const BASE_NEG2 = 5
export const ZIGZAG = 6
