import { copy } from 'https://deno.land/std/streams/conversion.ts'
import { SEEK_SET, SEEK_CUR, SEEK_END } from 'https://gitlab.com/lexxyfox/deno-fox/-/raw/foxxo/io/constants.js'

// Requires PERL
// Please help convert this to native JS!
// ... or at least to FFI using actual zlib

export default class InflateReader {
  #raw
  #proc
  #pos
  #initialization
  
  constructor(raw) {
    this.#pos = 0
    this.#raw = raw
    this.#initialization = this.#init()
  }
  
  close() {
    // stdin does NOT need to be closed here, 
    // but does stdout need to be?
    return this.#proc.close()
  }
  
  async #init() {
    this.#proc = Deno.run({
      cmd: ['perl', '-mIO::Uncompress::RawInflate=rawinflate', '-erawinflate"-","-"'],
      stdin: 'piped', stdout: 'piped', stderr: 'piped',
    })
    // todo: errors aren't captured (I think)
    copy(this.#raw, this.#proc.stdin).catch(async err => {
      const err_text = (new TextDecoder().decode(await this.#proc.stderrOutput()))
      if (err_text.length > 0)
        throw new Error(err_text)
    }).then(
      _ => this.#proc.stdin.close()
    )
  }
  
  async read(buffer) {
    await this.#initialization
    const bytes_read = await this.#proc.stdout.read(buffer)
    if (bytes_read === null)
      this.close()
    this.#pos += bytes_read
    return bytes_read
  }
  
  async skip_bytes(size = -1) {
    await this.#initialization
    if (size == 0)
      return 0
    else if (size < 0) {
      throw new Error('Not yet implemented!')
    } else if (size > 0) {
      let so_far = 0, b = 0
      while (b = await this.read(new Uint8Array(size - so_far))) {
        if (b == 0 || b == null)
          break
        so_far += b
      }
      this.#pos += so_far
      return so_far
    }
  }
  
  async seek(offset, whence = SEEK_SET) {
    if (whence == SEEK_SET)
      if (offset < this.#pos) // todo: this is possible
        throw new Error('Illegal seek')
      else if (offset == this.#pos)
        return this.#pos
      else {
        await this.skip_bytes(offset - this.#pos)
        return this.#pos
      }
    else if (whence == SEEK_CUR)
      if (offset < 0) // todo: this is possible
        throw new Error('Illegal seek')
      else if (offset == 0)
        return this.#pos
      else {
        await this.skip_bytes(offset)
        return this.#pos
      }
    else if (whence == SEEK_END)
      throw new Error('Illegal seek')
    throw new TypeError(`Invalid seek mode: ${whence}`)
  }
}
