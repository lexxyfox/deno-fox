// Math functions to use with the BigFrac class
// All functions should accept (if appropiate) Numbers, BigInts, or BigFracs
// JavaScript, Python, and Java were used as guides to function names
// After accuracy, speed is desired. I am not speed. Please help me!

import BigFrac from './BigFrac.js'

// PRIVATE UTILITY FUNCTIONS

const integer_only = function(a) {
  if (a instanceof BigFrac)
    if (a.den == 1n)
      return a.num
    else
      throw new TypeError('Argument must be an integer')
  return BigInt(a)
}

// takes bigfrac, bigint, or number; returns bigint
const get_num = function(a) {
  if (a instanceof BigFrac)
    return a.num
  if (typeof a === 'bigint')
    return a
  if (typeof a === 'number' && Number.isInteger(a))
    return BigInt(a)
  if (typeof a === 'number')
    return BigFrac(a).num
}

// takes bigfrac, bigint, or number; returns bigint
const get_den = function(a) {
  if (a instanceof BigFrac)
    return a.den
  if (typeof a === 'bigint')
    return 1n
  if (typeof a === 'number' && Number.isInteger(a))
    return 1n
  if (typeof a === 'number')
    return BigFrac(a).den
}

// This the glue that holds this entire library together.
export function gcd(a, b) {
  b = integer_only(b)
  const c = integer_only(a) % b
  return c == 0n ? b : gcd(b, c)
}

// By my rule as BDFL this function shall be officially named 'gcd'
// ... but I will allow this alias, just for funsies.
export const gcf = gcd

export function lcm(a, b) {
  a = integer_only(a), b = integer_only(b)
  if (a == 0n || b == 0n)
    return 0n
  let c = a * b
  if (c < 0n)
    c *= -1n
  return c / gcd(a, b)
}

// BASIC ARITHMETIC

export function add(a, b) { // a + b
  const a_num = get_num(a), a_den = get_den(a),
    b_num = get_num(b), b_den = get_den(b)
  if (a_den == b_den)
    return BigFrac(a_num + b_num, a_den)
  const c = lcm(a_den, b_den),
    d = (c / a_den) * a_num,
    e = (c / b_den) * b_num
  return BigFrac(d + e, c)
}

export function sub(a, b) { // a - b
  const a_num = get_num(a), a_den = get_den(a),
    b_num = get_num(b), b_den = get_den(b)
  if (a_den == b_den)
    return BigFrac(a_num - b_num, a_den)
  const c = lcm(a_den, b_den),
    d = (c / a_den) * a_num,
    e = (c / b_den) * b_num
  return BigFrac(d - e, c)
}

export function mul(a, b) { // a * b
  return BigFrac(get_num(a) * get_num(b), get_den(a) * get_den(b))
}

export function div(a, b) { // a / b
  return BigFrac(get_num(a) * get_den(b), get_den(a) * get_num(b))
}

export function floordiv(a, b) { // Math.floor(a / b)
  
}

export function mod(a, b) { // a % b
  
}

export function pow(a, b) { // a ** b
  
}

// COMPARISON OPERATORS
// these return a Boolean

export function lt(a, b) { // a < b
  const a_num = get_num(a), a_den = get_den(a),
    b_num = get_num(b), b_den = get_den(b),
    c = lcm(a_den, b_den)
  return (c / a_den) * a_num < (c / b_den) * b_num
}

export function le(a, b) { // a <= b
  const a_num = get_num(a), a_den = get_den(a),
    b_num = get_num(b), b_den = get_den(b),
    c = lcm(a_den, b_den)
  return (c / a_den) * a_num <= (c / b_den) * b_num
}

export function eq(a, b) { // a == b
  if (a == b)
    return true
  a = BigFrac(a), b = BigFrac(b)
  return a.den == b.den && a.num == b.num
}

export function ne(a, b) { // a != b
  return ! eq(a, b)
}

export function gt(a, b) { // a > b
  const a_num = get_num(a), a_den = get_den(a),
    b_num = get_num(b), b_den = get_den(b),
    c = lcm(a_den, b_den)
  return (c / a_den) * a_num > (c / b_den) * b_num
}

export function ge(a, b) { // a >= b
  const a_num = get_num(a), a_den = get_den(a),
    b_num = get_num(b), b_den = get_den(b),
    c = lcm(a_den, b_den)
  return (c / a_den) * a_num >= (c / b_den) * b_num
}

// returns -1 iff a < b, 0 iff a == b, 1 iff a > b
export function cmp(a, b) {
  
}

// UNARY OPERATORS

export function inc(a) { // ++ a
  
}

export function dec(a) { // -- a
  
}

export function pos(a) { // + a
  return a
}

export function neg(a) { // - a
  
}

export function abs(a) { // Math.abs(a)
  
}

// FRACTION -> INTEGER FUNCTIONS
// Returns integers

export function floor(a) { // Math.floor(a)
  
}

export function ceil(a) { // Math.ceil(a)
  
}

export function trunc(a) { // Math.trunc(a)
  
}

export function sign(a) { // Math.sign(a)
  
}

// PUESDO-BITWISE OPERATOR(S)
// I've decided not to fake all of them

export function lshift(a, b) { // a << b
  
}

export function rshift(a, b) {

}

// TRIGONOMETRIC OPERATORS
// Challenge mode activate!

export function sin(a) {
  
}

export function cos(a) {
  
}

export function tan(a) {
  
}

export function asin(a) {
  
}

export function acos(a) {
  
}

export function atan(a) {
  
}

export function sinh(a) {
  
}

export function cosh(a) {
  
}

export function tanh(a) {
  
}

export function asinh(a) {
  
}

export function acosh(a) {
  
}

export function atanh(a) {
  
}


