// Use to represent rationals of arbitrary precision.
// Also, y no support for operator overloading?!?!?

import * as BigMath from './BigMath.js'

const cache = new WeakMap()

// Internal function for use only with BigInts.
const new_big_frac = function(num, den) {
  if (den < 0)
    num *= -1n, den *= -1n
  const d = BigMath.gcd(num, den)
  if (d > 1)
    num = num / d, den = den / d
  if (!(den in cache))
    cache[den] = new WeakMap()
  if (num in cache[den])
    return cache[den][num]
  const val = (den == 1n)
    ? num
    : Object.defineProperties(Object.create(BigFrac.prototype), {
        'num': { value: num }, 'den': { value: den },
      })
  return cache[den][num] = val
}

// Constructs a BigFrac instance.
// The following is a list of my goals. It is a long list. Please help me!!!
// Either parameter may be passed in as:
//   * String (integer or fractional)
//   * Number (integer or fractional)
//   * BigInt
//   * BigFrac
// Custom string parsing - todo!
// If either parameter is NaN or null, then an error is thrown
// If denominator is Infinity, then numerator is ignored and BigFrac(0, 1) is returned
// If denominator is zero, then an error is thrown (similar to BigInt).
// If denominator is one, then a BigInt is returned
// If the fraction can be safely stored as a native float, then a float is returned - todo!
// Otherwise, a BigFrac is returned with simplest terms and a positive whole denominator
export default function BigFrac(num, den = 1n) {
  if (num >= Infinity)
    throw new TypeError('Infinity is not a rational number')
  if (den >= Infinity)
    return new_big_frac(0n, 1n)
  if (den == 0)
    return Infinity
  if (num === null || num === NaN || num === undefined)
    throw new TypeError(`Numerator cannot be ${num}`)
  if (den === null || den === NaN)
    throw new TypeError(`Denominator cannot be ${num}`)
  if (num instanceof BigFrac) // incomplete, must adjust denominator
    den = num.den, num = num.num
  if (typeof den !== 'bigint') // incomplete, must test for BigFrac
    if (typeof den === 'string' && den.indexOf('.') < 0)
      throw new Error('Fractional denominators are not yet implemented!')
    else if (typeof den === 'number' && ! Number.isInteger(den))
      throw new Error('Fractional denominators are not yet implemented!')
    else
      den = BigInt(den)
  if (typeof num !== 'bigint') // incomplete, must test for BigFrac
    if (typeof num === 'string' && num.indexOf('.') < 0)
      num = BigInt(num)
    else if (typeof num === 'string') { // todo - rounding!
      const l = 10**(num.length - num.indexOf('.') - 1)
      num = BigInt(num * l), den *= BigInt(l) // todo - limited precision
    } else if (typeof num === 'number' && ! Number.isInteger(num)) {
      const s = num.toString(),
        l = 10**(s.length - s.indexOf('.') - 1) // todo - limited precision
      num = BigInt(num * l), den *= BigInt(l)
    } else
      num = BigInt(num)
  return new_big_frac(num, den)
}

BigFrac.prototype.toString = function(radix = 10) {
  return this.num.toString(radix) + '/' + this.den.toString(radix)
}

BigFrac.prototype.toJSON = function() {
  
}

BigFrac.prototype.toNumber = function() {
  
}

BigFrac.prototype.toUnicode = function(radix = 10) {
  
}

if (typeof Deno !== 'undefined')
  BigFrac.prototype[Deno.customInspect] = function() {
    return this.toString()
  }
