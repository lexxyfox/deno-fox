# Foxxy's ~~Mighty Morphin' Fr-actionals~~ BigFracs

BigFracs can represent arbitrary [rational numbers](https://en.wikipedia.org/wiki/Rational_number) with aribtray precision. Yes, really... arbitrary precision. As long as it's a rational number. Arbitrary.

```
import BigFrac from 'https://gitlab.com/lexxyfox/deno-fox/raw/foxxo/math/BigFrac.js'
```

- Not like [Java's BigDecimal](https://docs.oracle.com/javase/8/docs/api/java/math/BigDecimal.html), nope. Those are limited floating point values.
- Trigonometric values are NOT rational - so they will be approximated.
- Also does not do [symbolic math](https://en.wikipedia.org/wiki/Computer_algebra). If you need symbols, use a different library.
- Arbitrary precision rational numbers only.
- Backed by ~~CoinCoin™ smart contracts~~ native [Javascript BigInts](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/BigInt). 

## ~~Operator overloading!!~~

~~Just like BigInts, BigFracs may be used with the + * - % ** operators.~~

Haha nope, Javascript doesn't support operator overloading (yet!)

## Or does it?!?!

BigFracs support the equality operators.

```
BigFrac(2, 3) == BigFrac(2, 3) // true
BigFrac(2, 3) === BigFrac(2, 3) // also true!
```

Like BigInts, BigFracs are loosely equal to their Number equivalent.

```
BigFrac(4, 2) == 2 // true
```

But also strictly equal to their BigInt equivalent.

```
BigFrac(4, 2) === 2n // true
BigFrac(4, 2) === 2 // false
```

Unlike BigInts, comparison with Object-wrapped BigFracs displays undefined behaviour. Avoid at all costs.

```
BigFrac(3, 5) === Object(BigFrac(3, 5)) // true
BigFrac(4, 2) === Object(BigFrac(2, 1)) // false
```

Huh well, hopefully that doesn't break anything later I guess. Why are you wrapping numbers in an Object anywho??

## Fractions, simplified

BigFracs are easy to construct! You can pass in two BigInts: a numerator and a denominator.

```
const x = BigFrac(2n, 3n)
```

Regular Numbers work just fine too.

```
const x = BigFrac(3, 5)
```

Let's mix it up a little!

```
const x = BigFrac(5n, 8)
```

If it's a whole number you want

```
const x = BigFrac(8)
```

I suppose that's kinda silly... but what about decimal values?

```
BigFrac(3.14) == BigFrac(314, 100) // true
BigFrac(2.5, 5) == BigFrac(1, 2) // true
```

Strings too?

```
BigFrac('7', 4) == BigFrac('1.75') // true
```

Even other radices?

```
BigFrac('0b111', 4) == BigFrac('1.75') // true
```

Zero possibilities, infinite limitations.

```
BigFrac(0, 4) == 0 // true
BigFrac(1.61, 0) == Infinity // true
BigFrac(1.61, Infinity) == 0 // true
BigFrac(Infinity, 2) // error!
```

## But can it maths?

Yes.

Maybe a little.

```
import BigMath from 'https://gitlab.com/lexxyfox/deno-fox/raw/foxxo/math/BigMath.js'
```

Many BigMath functions, where appropriate, can handle any combination of integer Numbers, float Numbers, BigInts, and BigFracs. This almost entirely elimitates the need to instantiate BigFracs manually.

```
const x = BigMath.mul(Number.MAX_SAFE_INTEGER, BigMath.add(Math.PI, Math.E))
```

There's a lot of functions that do things! This is going to be a big part of the documentation some day.

Basic arithmetic

- add(ℚ, ℚ) → ℚ
- sub(ℚ, ℚ) → ℚ
- mul(ℚ, ℚ) → ℚ
- div(ℚ, ℚ) → {∞ iff divisor = 0, ℚ otherwise}
- floordiv(ℚ, ℚ) → ℤ
- mod(ℚ, ℚ) → ℚ
- pow(ℚ, ℚ) → {ℚ iff exponent is ℤ, ~ℚ otherwise}

Comparison operators

- lt(ℚ, ℚ) → Bool
- le(ℚ, ℚ) → Bool
- eq(ℚ, ℚ) → Bool
- ne(ℚ, ℚ) → Bool
- gt(ℚ, ℚ) → Bool
- ge(ℚ, ℚ) → Bool
- cmp(ℚ, ℚ) → {-1, 0, or 1}

Unary operators

- inc(ℚ) → ℚ
- dec(ℚ) → ℚ
- pos(ℚ) → ℚ
- neg(ℚ) → ℚ
- abs(ℚ) → ℚ ≥ 0

Integer functions

- floor(ℚ) → ℤ
- ceil(ℚ) → ℤ
- trunc(ℚ) → ℤ
- sign(ℚ) → {-1, 0, or 1}

Puesdo-bitwise functions

- lshift(ℚ, ℤ) → ℚ
- rshift(ℚ, ℤ) → ℚ

Trig

- sin(ℚ) → ~ℚ
- cos(ℚ) → ~ℚ
- tan(ℚ) → ~ℚ

Inverse trig

- a*(ℚ) → ~ℚ

Hyperbolic trig

- *h(ℚ) → ~ℚ

Inverse hyperbolic trig

- a*h(ℚ) → ~ℚ



