// https://github.com/mvolkmann/star-it

/**
 * Yields _up to_ the next n values from the generator obj
 * @param {Generator<T, TReturn, TNext>} obj
 * @param {number} n
 * @returns {Generator<T, TReturn, TNext>}
 */
export const take = function* (obj, n) {
  const iterator = obj[Symbol.iterator]()
  for (; n > 0; --n) {
    const next = iterator.next()
    if (next.done) return next.value
    yield next.value
  }
}
