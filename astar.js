// https://github.com/mvolkmann/star-it

/**
 * Yields _up to_ the next n values from the async generator obj
 * @param {AsyncGenerator<T, TReturn, TNext>} obj
 * @param {number} n
 * @returns {AsyncGenerator<T, TReturn, TNext>}
 */
export const take = async function* (obj, n) {
  const iterator = obj[Symbol.asyncIterator]()
  for (; n > 0; --n) {
    const { value, done } = await iterator.next()
    if (done) return value
    yield value
  }
}

/**
 * Calls a defined callback function on each element yieled by an async generator,
    and returns a generator that yields the results.
 * @param {AsyncGenerator<T, TReturn, TNext>} obj
 * @param {Function} callbackfn
 * @returns {AsyncGenerator<T, TReturn, TNext>}
 */
export const map = async function* (obj, callbackfn) {
  for await (const element of obj) yield callbackfn(element)
}

// https://github.com/tc39/proposal-array-from-async

/**
 * Returns an array containing all the resolved values of the Iterator iter.
 * Polyfill for the proposed Array.asyncFrom
 * @param {AsyncIterator<T, TReturn, TNext>} iter
 * @returns {Promise<Array<T | TReturn>>}
 */
export const list = async iter => {
  const arr = []
  for await (const entry of iter) arr.push(entry)
  return arr
}
