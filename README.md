Goals / Philosophies / Reasoning:
* ????
* Use promises! Callbacks are bad, m'kay?
* Read and write lots of formats
* Be compatible with the JAVASCRIPT API! (the es6+ kind). This is why I chose Deno.
* Try to limit data memory usage.
* Consider all other programming languages when designing API (but assume these hypothetical languages have async/await functionality).
* Prefer file api (.read(buf) / .write(buf)) over iterator style io (like the fetch api).
* Consider arbitrary byte endiannesses, and arbitrary signednesses.
* Use TypeScript or whatever future typing annotations when ratified by ECMA and supported by the leading FLOSS web browser at the time.
* Say YES on barrelling.
* Learn JavaScript and have fun :3
