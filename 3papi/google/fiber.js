import { DOMParser } from 'https://gitlab.com/lexxyfox/deno-fox/raw/foxxo/w3c/domparsing.js'

const parser = new DOMParser()

/**
 * @param {string} street_address
 * @param {string|number} zip_code
 * @param {string|number} [unit_number]
 */
export const has_fiber = async (street_address, zip_code, unit_number = '') =>
  !!parser
    .parseFromString(
      await (
        await fetch('https://fiber.google.com/address/', {
          method: 'POST',
          body: new URLSearchParams({
            street_address: street_address,
            unit_number: unit_number,
            zip_code: zip_code,
          }),
        })
      ).text(),
      'text/html',
    )
    .querySelector('body > modularsignup-app')
