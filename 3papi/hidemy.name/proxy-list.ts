import { DOMParser } from 'https://gitlab.com/lexxyfox/deno-fox/raw/foxxo/w3c/domparsing.js'
import { Element } from 'https://gitlab.com/lexxyfox/deno-fox/raw/foxxo/w3c/dom.js'

export const DEFAULT_URL = 'https://hidemy.name/en/proxy-list'

export const ANON_UNKNOWN = -1
export const ANON_NONE = 0
export const ANON_LOW = 1
export const ANON_AVERAGE = 2
export const ANON_HIGH = 3

export const MASK_NONE = 1
export const MASK_LOW = 2
export const MASK_AVERAGE = 4
export const MASK_HIGH = 8
export const MASK_ANY = 15

export const TYPE_HTTP = 1
export const TYPE_HTTPS = 2
export const TYPE_SOCKS4 = 4
export const TYPE_SOCKS5 = 8
export const TYPE_ANY = 15

export type AnonLevel = -1 | 0 | 1 | 2 | 3

export type BitField4 =
  | 0
  | 1
  | 2
  | 3
  | 4
  | 5
  | 6
  | 7
  | 8
  | 9
  | 10
  | 11
  | 12
  | 13
  | 14
  | 15

export class Entry implements Deno.NetAddr {
  anonymity: AnonLevel
  city: string | null
  country: string
  hostname: Deno.NetAddr['hostname']
  last_update: number | null
  latency: number
  port: Deno.NetAddr['port']
  transport: Deno.NetAddr['transport']
  types: BitField4
  constructor(
    hostname: string,
    port: number,
    country: string,
    city: string | null,
    latency: number,
    types: BitField4,
    anonymity: AnonLevel,
    last_update: number | null,
  ) {
    this.hostname = hostname
    this.port = port
    this.country = country
    this.city = city
    this.latency = latency
    this.types = types
    this.anonymity = anonymity
    this.last_update = last_update
    this.transport = 'tcp'
  }
  get urls() {
    const set = new Set() as Set<string>
    const part = `${this.hostname}:${this.port}`
    if (this.types & TYPE_HTTP) set.add('http://' + part)
    if (this.types & TYPE_HTTPS) set.add('https://' + part)
    if (this.types & TYPE_SOCKS4) set.add('socks4://' + part)
    if (this.types & TYPE_SOCKS5) set.add('socks5://' + part)
    return set
  }
}

const parse_anonymity = (str: string): AnonLevel => {
  switch (str) {
    case 'High':
      return ANON_HIGH
    case 'Average':
      return ANON_AVERAGE
    case 'Low':
      return ANON_LOW
    case 'no':
      return ANON_NONE
    default:
      return ANON_UNKNOWN
  }
}

const parse_ago = (str: string) => {
  if (str.slice(-4) === 'min.') {
    const dot = str.indexOf('.')
    return (
      parseInt(str.slice(0, dot - 2)) * 3600 +
      parseInt(str.slice(dot + 2, -5)) * 60
    )
  }
  const num = parseInt(str.slice(0, -8)),
    unit = str.slice(-7)
  return unit === 'seconds' ? num : unit === 'minutes' ? num * 60 : null
}

const parse_type = (str: string) => {
  switch (str) {
    case 'SOCKS5':
      return TYPE_SOCKS5
    case 'SOCKS4':
      return TYPE_SOCKS4
    case 'HTTPS':
      return TYPE_HTTPS
    case 'HTTP':
      return TYPE_HTTP
    default:
      return 0
  }
}

const parse_types = (str: string) => {
  const split = str.split(', ')
  let val = 0
  for (const t of split) val |= parse_type(t)
  return val as BitField4
}

const raw_get = async (
  url: string = DEFAULT_URL,
  params = {},
  fetcher: typeof fetch = fetch,
) => {
  const params2 = new URLSearchParams(params).toString()
  if (params2) url += '/?' + params2
  const resp = await fetcher(url)

  const doc = new DOMParser().parseFromString(await resp.text(), 'text/html')!
  const epoch = Date.parse(resp.headers.get('date') as string) / 1000

  return Array.from(
    doc.querySelectorAll('div.table_block > table > tbody > tr'),
  ).map(x => {
    const children = (x as Element).children
    const ago = parse_ago(children[6].textContent),
      city = children[2].querySelector('.city')!.textContent

    return new Entry(
      children[0].textContent,
      parseInt(children[1].textContent),
      children[2].querySelector('.country')!.textContent,
      city ? city : null,
      Math.ceil(parseInt(children[3].textContent.slice(0, -3)) / 20),
      parse_types(children[4].textContent),
      parse_anonymity(children[5].textContent),
      ago ? epoch - ago : null,
    )
  })
}

export type ListOptions = {
  anon?: BitField4
  fetcher?: typeof fetch
  maxtime?: number
  start?: number
  type?: BitField4
  url?: string
}

export const get_list = async function* (
  options: ListOptions = {},
): AsyncGenerator<Entry, void, undefined> {
  const opts2: {
    anon?: string
    maxtime?: number
    start: number
    type?: string
    url?: string
  } = {
    start: options.start ?? 0,
  }
  const url = options.url ?? DEFAULT_URL
  const fetcher = options.fetcher ?? fetch
  if (options.maxtime) opts2.maxtime = options.maxtime * 20
  if (options.anon && options.anon != MASK_ANY) {
    opts2.anon = ''
    if (options.anon & MASK_NONE) opts2.anon += '1'
    if (options.anon & MASK_LOW) opts2.anon += '2'
    if (options.anon & MASK_AVERAGE) opts2.anon += '3'
    if (options.anon & MASK_HIGH) opts2.anon += '4'
  }
  if (options.type && options.type != TYPE_ANY) {
    opts2.type = ''
    if (options.type & TYPE_HTTP) opts2.type += 'h'
    if (options.type & TYPE_HTTPS) opts2.type += 's'
    if (options.type & TYPE_SOCKS4) opts2.type += '4'
    if (options.type & TYPE_SOCKS5) opts2.type += '5'
  }

  for (;;) {
    const results = await raw_get(url, opts2, fetcher)
    if (results.length <= 0) return
    opts2.start += results.length
    yield* results
  }
}
