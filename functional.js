// https://underscorejs.org/

/**
 * @param {T | Array<T>} x
 * @returns {Array<T>}
 */
export const arrayify = x => (Array.isArray(x) ? x : [x])

const deepGet = (obj, path) => {
  const len = path.length
  for (let i = 0; i < len; i++)
    if (obj == null) return void 0
    else obj = obj[path[i]]
  return len ? obj : void 0
}

export const property = path => {
  path = arrayify(path)
  return obj => deepGet(obj, path)
}
