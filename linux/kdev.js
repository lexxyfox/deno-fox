// https://elixir.bootlin.com/linux/latest/source/tools/include/nolibc/nolibc.h#L2555
// https://elixir.bootlin.com/linux/latest/source/include/linux/kdev_t.h

/**
 * 24 bit major + 8 bit minor -> 32 bit dev
 * @param {number} major
 * @param {number} minor
 * @returns {number} dev
 */
export const old_unparse_dev = (major, minor) => (minor & 0xff) | (major << 8)

/**
 * 32 bit dev -> 24 bit major + 8 bit minor
 * @param {number} dev
 */
export const old_parse_dev = dev => ({ major: dev >> 8, minor: dev & 0xff })

/**
 * 18 bit major + 14 bit minor -> 32 bit dev
 * @param {number} major
 * @param {number} minor
 * @returns {number} dev
 */
export const sysv_unparse_dev = (major, minor) =>
  (minor & 0x3fff) | (major << 14)

/**
 * 32 bit dev -> 18 bit major + 14 bit minor
 * @param {number} dev
 */
export const sysv_parse_dev = dev => ({ major: dev >> 14, minor: dev & 0x3fff })

/**
 * 12 bit major + 20 bit minor -> 32 bit dev
 * @param {number} major
 * @param {number} minor
 * @returns {number} dev
 */
export const new_unparse_dev = (major, minor) =>
  (minor & 0xff) | ((major & 0xfff) << 8) | ((minor & 0xfff00) << 12)

/**
 * 32 bit dev -> 12 bit major + 20 bit minor
 * @param {number} dev
 */
export const new_parse_dev = dev => ({
  major: (dev >> 8) & 0xfff,
  minor: (dev & 0xff) | ((minor >> 12) & 0xfff00),
})

// https://code.woboq.org/gtk/include/sys/sysmacros.h.html

/**
 * Currently preferred method of converting from major and minor to device id.
 * 32 bit major + 32 bit minor -> 64 bit dev
 * @param {number|BigInt} major
 * @param {number|BigInt} minor
 * @returns {BigInt} dev
 */
export const gnu_unparse_dev = (major, minor) =>
  (BigInt(minor) & 0xffn) +
  (BigInt(major) & 0xfffn) * 256n +
  (BigInt(minor) & 0xffffff00n) * 4096n +
  (BigInt(major) & 0xfffff000n) * 4294967296n

/**
 * Currently preferred method of converting from device id to major and minor.
 * 64 bit dev -> 32 bit major + 32 bit minor
 * @param {number|BigInt} dev
 */
export const gnu_parse_dev = dev => ({
  major:
    Number((BigInt(dev) / 256n) & 0xfffn) +
    Number((BigInt(dev) / 4294967296n) & 0xfffff000n),
  minor:
    Number(BigInt(dev) & 0xffn) + Number((BigInt(dev) / 4096n) & 0xffffff00n),
})
