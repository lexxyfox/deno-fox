import { readAll } from 'https://deno.land/std/streams/conversion.ts'
import * as pathlib from 'https://deno.land/std/path/posix.ts'



// OBJECT-ORIENTED INTERFACE. Use with remote systems.

export class ProcFS {
  #filesystem
  #path
  constructor(path = '/proc', filesystem = Deno) {
    this.#filesystem = filesystem
    this.#path = path
  }
  async cpuinfo() {
    return cpuinfo(pathlib.join(this.#path, 'cpuinfo'), this.#filesystem)
  }
  async uptime() {
    return uptime(pathlib.join(this.#path, 'uptime'), this.#filesystem)
  }
  async loadavg() {
    return loadavg(pathlib.join(this.#path, 'loadavg'), this.#filesystem)
  }
  async cmdline() {
    return cmdline(pathlib.join(this.#path, 'cmdline'), this.#filesystem)
  }
  async partitions() {
    return partitions(pathlib.join(this.#path, 'partitions'), this.#filesystem)
  }
}



// PRIVATE STUFF. Highly unstable, don't use!

const readText = async (path, filesystem = Deno) => 
  new TextDecoder().decode(await readAll(await filesystem.open(path)))

const cpuinfo_int_keys = new Set(['cpu_family', 'model', 'stepping', 'physical_id', 'siblings', 'core_id', 'cpu_cores', 'apicid', 'initial_apicid', 'cpuid_level', 'clflush_size', 'cache_alignment', 'microcode'])
const cpuinfo_bool_keys = new Set(['fpu', 'fpu_exception', 'wp'])
const cpuinfo_set_keys = new Set(['flags', 'bugs'])
const cpuinfo_float_keys = new Set(['bogomips'])



// PROCEDURAL INTERFACE. Defaults to current system. Probably stable.

// https://elixir.bootlin.com/linux/latest/source/arch/x86/kernel/cpu/proc.c#L61
export async function cpuinfo(path = '/proc/cpuinfo', filesystem = Deno) {
  const text = await readText(path, filesystem)
  const cpus = []
  for (const cpu_text of text.split('\n\n')) 
    if (cpu_text) {
      const cpu = {}
      for (const cpu_line of cpu_text.split('\n')) {
        const pos = cpu_line.indexOf(':')
        const key = cpu_line.slice(0, pos).trim().replaceAll(' ', '_').toLowerCase()
        const val = cpu_line.slice(pos + 1).trim()
        if (key === 'processor')
          continue
        else if (cpuinfo_int_keys.has(key))
          cpu[key] = parseInt(val)
        else if (cpuinfo_bool_keys.has(key))
          cpu[key] = val == 'yes' ? true : false
        else if (cpuinfo_float_keys.has(key))
          cpu[key] = parseFloat(val)
        else if (cpuinfo_set_keys.has(key))
          cpu[key] = new Set(val.split(' '))
        else if (key === 'cpu_mhz') 
          cpu['frequency'] = parseFloat(val) * 1000000
        else if (key === 'cache_size')
          cpu[key] = val.endsWith('KB') ? parseInt(val.slice(0, -2)) * 1024 : val
        else if (key === 'tlb_size')
          cpu[key] = parseInt(val.slice(0, -9))
        else if (key === 'address_sizes') {
          cpu['address_sizes'] = {}
          for (const spec of val.split(',')) {
            const parts = spec.trim().split(' ')
            cpu['address_sizes'][parts[2]] = parseInt(parts[0])
          }
        } else if (key === 'power_management') {
          cpu['power_management'] = new Set()
          for (const x of val.split(' '))
            cpu['power_management'].add(
              x.startsWith('[') ? parseInt(x.slice(1, -1)) : x
            )
        } else
          cpu[key] = val
      }
      cpus.push(cpu)
    }
  return cpus
}

export async function meminfo(path = '/proc/meminfo', filesystem = Deno) {
  const text = await readText(path, filesystem)
  const info = {}
  for (const line of text.split('\n'))
    if (line) {
      const pos = line.indexOf(':')
      const key = line.slice(0, pos).trim()
      let val = line.slice(pos + 1).trim()
      val = val.endsWith('kB') ? parseInt(val.slice(0, -2)) * 1024 : parseInt(val)
      info[key] = val
    }
  return info
}

export async function uptime(path = '/proc/uptime', filesystem = Deno) {
  const parts = (await readText(path, filesystem)).split(' ')
  return {
    uptime: parseFloat(parts[0]),
    idle  : parseFloat(parts[1]),
  }
}

export async function loadavg(path = '/proc/loadavg', filesystem = Deno) {
  const parts = (await readText(path, filesystem)).split(' ')
  const pos = parts[3].indexOf('/')
  return {
    avenrun: [parseFloat(parts[0]), parseFloat(parts[1]), parseFloat(parts[2])],
    nr_running: parseInt(parts[3].slice(0, pos)),
    nr_threads: parseInt(parts[3].slice(pos + 1)),
    idr_cursor: parseInt(parts[4])
  }
}

export async function cmdline(path = '/proc/cmdline', filesystem = Deno) {
  return (await readText(path, filesystem)).slice(0, -1)
}

// https://elixir.bootlin.com/linux/latest/source/block/genhd.c#L826
export const partitions = async (path = '/proc/partitions', filesystem = Deno) => {
  const lines = (await readText(path, filesystem)).split('\n').slice(2, -1)
  const parts = []
  for (const line of lines) {
    parts.push({
      name: line.substring(25),
      blocks: parseInt(line.substring(13, 24)),
      major: parseInt(line.substring(0, 4)),
      minor: parseInt(line.substring(4, 13)),
    })
  }
  return parts
}



// FAKE SYSCALL EMULATION. Functions are probably stable,
// ...BUT might be moved to another module.

export async function avenrun(filesystem = Deno) {
  const loads = (await loadavg(undefined, filesystem)).avenrun
  return [
    Math.round(loads[0] * 200),
    Math.round(loads[1] * 200),
    Math.round(loads[2] * 200)
  ]
}

// https://man7.org/linux/man-pages/man2/sysinfo.2.html
export async function sysinfo(filesystem = Deno) {
  const [mi, uptime, loadavg] = Promise.all([
    meminfo(undefined, filesystem),
    uptime(undefined, filesystem),
    loadavg(undefined, filesystem),
  ])
  return {
    uptime   : uptime.uptime,
    loads    : loadavg.avenrun,
    totalram : mi.MemTotal,
    freeram  : mi.MemFree,
    sharedram: mi.Shmem,
    bufferram: mi.Buffers,
    totalswap: mi.SwapTotal,
    freeswap : mi.SwapFree,
    procs    : null, // todo
    totalhigh: mi.HighTotal,
    freehigh : mi.HighFree,
    mem_unit : null, // todo
  }
}

