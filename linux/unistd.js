const rid = Deno.core.opSync('op_ffi_load', {
  path: '', symbols: {
    'getuid': {
      'parameters': [],
      'result': 'u32',
    }
  }
})

const op_ffi_call = Deno.core.ops()['op_ffi_call']

export function getuid() {
  return Deno.core.opcallSync(op_ffi_call, {
    rid: rid,
    symbol: 'getuid',
    parameters: [],
  })
}

