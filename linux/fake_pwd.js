export async function getpwents() {
  const entries = []
  for (const line of (await Deno.readTextFile('/etc/passwd')).split('\n')) 
    if (line.length > 0) {
      const parts = line.split(':')
      entries.push({
        'name'  : parts[0],
        'passwd': parts[1],
        'uid'   : parseInt(parts[2]),
        'gid'   : parseInt(parts[3]),
        'gecos' : parts[4].length > 0 ? parts[4] : null,
        'dir'   : parts[5],
        'shell' : parts[6],
      })
    }
  return entries
}
